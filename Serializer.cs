﻿using System;
using System.Dynamic;
using System.Reflection;



namespace HomeWorkOtusReflection
{
    public class Serializer
    {
        public static string ToCSV<T>(T obj)
        {
            string str = "";
            Type myType = obj.GetType();
            var fields = myType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

            foreach (var field in fields)
                str += field.Name + ":" + field.GetValue(obj)+ ",";

            return str;
        }

        public static T FromCSV<T>(string str)
        {
            FieldInfo field;
            Type t = typeof(T);
            var obj = t.GetConstructor(new Type[] { }).Invoke(new object[] { });
            var propList = str.Split(',');
            string[] prop;

            foreach (string s in propList)
            {
                prop = s.Split(':');
                if (s == "") break;
                field = t.GetField(prop[0], BindingFlags.Instance | BindingFlags.NonPublic);
                switch (field.FieldType.Name)
                {
                    case "Int32":
                        field.SetValue(obj, Convert.ToInt32(prop[1]));
                        break;
                    default:
                        field.SetValue(obj, prop[1]);
                        break;
                }
            }

            return (T)obj;
        }
    }
}
