﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWorkOtusReflection
{
    public class Subject
    {
        int x, y, z, index, lenght;
        string s;
        public static Subject Get() => new Subject() { x = 1, y = 2, z = 3, index = 4, lenght = 5, s = "serializer" };

        public void ShowProp()
        {
            Console.WriteLine("x: {0}", x);
            Console.WriteLine("y: {0}", y);
            Console.WriteLine("z: {0}", z);
            Console.WriteLine("index: {0}", index);
            Console.WriteLine("lenght: {0}", lenght);
            Console.WriteLine("s: {0}", s);
        }


    }
}
