﻿using System;
using System.Diagnostics;

namespace HomeWorkOtusReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringCSV = "";
            Subject subjectFromCSV = Subject.Get();

            Subject subject = Subject.Get();

            subject.ShowProp();
            Console.WriteLine();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i < 100000; i++ )
                stringCSV = Serializer.ToCSV(subject);

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            Console.WriteLine("Время на сериализацию в CSV: {0}", ts);
            Console.WriteLine();

            Console.WriteLine(stringCSV);
            Console.WriteLine();

            stopWatch.Reset();
            stopWatch.Start();

            for (int i = 0; i < 100000; i++)
                subjectFromCSV = Serializer.FromCSV<Subject>(stringCSV);

            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            Console.WriteLine("Время на десериализацию из CSV: {0}", ts);
            Console.WriteLine();

            subjectFromCSV.ShowProp();
        }
    }
}
